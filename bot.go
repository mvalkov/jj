package main

import (
	"bytes"
	"database/sql"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/mikkyang/id3-go"

	"github.com/meehalkoff/loggi"

	tb "gopkg.in/tucnak/telebot.v2"
)

var (
	bot       *tb.Bot
	err       error
	musChanel *tb.Chat
	db        *sql.DB
)

func InitBot(d *sql.DB) {
	bot, err = tb.NewBot(tb.Settings{
		Token: *TOKEN,
	})
	if err != nil {
		loggi.Fatal(err)
	}
	musChanel = &tb.Chat{ID: *CID}
	db = d
}

func SendSong(song *Song) {

	if songIsExist(song.Name) {
		oldSong, _ := getSongByName(song.Name, db)
		if song.Size > oldSong.Size {
			err := send(song)
			if err == nil {
				oldMsg := tb.StoredMessage{MessageID: oldSong.MessageId, ChatID: *CID}
				bot.Delete(oldMsg)
				updateSong(song, db)
			}
		}
	} else {
		err := send(song)
		if err == nil {
			insertSong(song, db)
		}
	}
	os.Remove(song.path)
	loggi.Debug(song)
}

func songIsExist(name string) bool {
	if _, err := getSongByName(name, db); err != nil {
		return false
	}
	return true
}

func send(song *Song) error {

	if !checkSize(song.path) {
		os.Remove(song.path)
		loggi.Debug("wrong file size. sending stop")
		return fmt.Errorf("wrong file size")
	}

	loggi.Debug("start sending", song.path)

	f, err := id3.Open(song.path)
	if err != nil {
		loggi.Debug("IO read file error:", song.path)
	}
	performer := f.Artist()
	title := f.Title()

	loggi.Debug("read id3: ", performer, " -- ", title)

	loggi.Debug("getDuration: ", getDuration(song.path))

	f.Close()

	audio := &tb.Audio{
		File:    tb.FromDisk(song.path),
		Caption: *TAG + "\n" + *CHANNAME,
		// Title:     title,
		// Performer: performer,
		// MIME:      "audio/aac",
		// Duration:  getDuration(song.path),
	}
	message, err := bot.Send(musChanel, audio)
	loggi.Debug(message)

	if err != nil {
		loggi.Fatal(err)
	}

	song.TgFileID = audio.FileID
	song.MessageId, _ = message.MessageSig()

	os.Remove(song.path)
	loggi.Debug("sended")
	time.Sleep(time.Second * 5)
	return nil
}

func checkSize(path string) bool {
	var (
		megabyte    = int64(1048576)
		minFileSize = 2 * megabyte
		maxFileSize = 50 * megabyte
	)

	fs, err := os.Stat(path)
	if err != nil {
		loggi.Fatal(err)
	}

	if fs.Size() > maxFileSize || fs.Size() < minFileSize {
		return false
	}

	return true
}

func getDuration(filepath string) int {
	args := []string{
		"-v", "quiet",
		"-print_format", "compact=print_section=0:nokey=1:escape=csv",
		"-show_entries", "format=duration",
		filepath,
	}

	cmd := exec.Command("ffprobe", args...)

	var stderr, stdout bytes.Buffer
	cmd.Stderr = &stderr
	cmd.Stdout = &stdout
	cmd.Start()
	cmd.Wait()

	fl, _ := strconv.ParseFloat(strings.TrimSpace(stdout.String()), 2)

	return int(fl)

}

// ffprobe -i <file> -show_entries format=duration -v quiet -of csv="p=0"
